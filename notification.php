<?php

class FactionNotification {

  protected $api;

  public function __construct($api) {
    $this->api = $api;
  }

  public function list_all($data) {
    return $this->api->send('GET', array('notifications'), array('body' => $data));
  }

  public function get($notification_id) {
    return $this->api->send('GET', array('notifications', $notification_id));
  }

  public function seen($notification_id) {
    return $this->api->send('DELETE', array('notifications', $notification_id));
  }

  public function create($type, $group, $priority, $data) {
    $body = array(
      'type' => $type,
      'group' => $group,
      'priority' => $priority,
      'info' => $data,
    );

    return $this->api->send('POST', array('notifications'), array('body' => $body));
  }
}
