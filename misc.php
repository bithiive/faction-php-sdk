<?php

class FactionMisc {

  protected $api;

  public function __construct($api) {
    $this->api = $api;
  }

  public function export($data) {
    return $this->api->send('GET', array('export'), array('body' => $data));
  }

  public function import($data) {
    return $this->api->send('POST', array('import'), array('body' => $data));
  }
}
