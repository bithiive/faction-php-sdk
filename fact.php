<?php

class FactionFact {

  protected $api;

  public function __construct($api) {
    $this->api = $api;
  }

  public function types() {
    return $this->api->send('GET', '/facts');
  }

  public function list_all($type, $data = NULL) {
    $options = array(
      'body' => $data
    );

    if (isset($options['body']['paging'])) {
      $options['query'] = $options['body']['paging'];
      unset($options['body']['paging']);
    }

    return $this->api->send('GET', array('facts', $type), $options);
  }

  public function get($type, $id, $data = NULL) {
    return $this->api->send('GET', array('facts', $type, 'fact', $id), array('body' => $data));
  }

  public function update($type, $id, $data) {
    return $this->api->send('POST', array('facts', $type, 'fact', $id), array('body' => $data));
  }

  public function remove($type, $id) {
    return $this->api->send('DELETE', array('facts', $type, 'fact', $id));
  }

  public function remove_all($type, $id) {
    return $this->api->send('DELETE', array('facts', $type));
  }

  public function trigger_update($type, $id) {
    return $this->api->send('POST', array('facts', $type, 'update', $id));
  }

  public function trigger_update_all($type, $id, $data = NULL) {
    return $this->api->send('POST', array('facts', $type, 'update'), array('body' => $data));
  }

  public function get_settings($type) {
    return $this->api->send('GET', array('facts', $type, 'settings'));
  }

  public function update_settings($type, $data) {
    return $this->api->send('POST', array('facts', $type, 'settings'), array('body' => $data));
  }

  public function remove_settings($type) {
    return $this->api->send('DELETE', array('facts', $type, 'settings'));
  }

  public function get_hooks($type) {
    return $this->api->send('GET', array('facts', $type, 'hooks'));
  }

  public function update_hook($type, $id, $data) {
    return $this->api->send('POST', array('facts', $type, 'hooks', $id), array('body' => $data));
  }

  public function remove_hook($type, $id) {
    return $this->api->send('DELETE', array('facts', $type, 'hooks', $id));
  }

  public function get_metrics($type) {
    return $this->api->send('GET', array('facts', $type, 'metrics'));
  }

  public function get_metric($type, $id, $data) {
    return $this->api->send('GET', array('facts', $type, 'metrics', $id), array('body' => $data));
  }

  public function update_metric($type, $id, $data) {
    return $this->api->send('POST', array('facts', $type, 'metrics', $id), array('body' => $data));
  }

  public function remove_metric($type, $id) {
    return $this->api->send('DELETE', array('facts', $type, 'metrics', $id));
  }

  public function test_metric($type, $id) {
    return $this->api->send('GET', array('facts', $type, 'metrics', $id, 'test'));
  }
}
