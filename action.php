<?php

class FactionAction {

  protected $api;

  public function __construct($api) {
    $this->api = $api;
  }

  public function types() {
    return $this->api->send('GET', array('/action-types'));
  }

  public function list_all($fact_type = NULL, $options) {
    return $this->api->send('GET', array('actions', $fact_type), $options);
  }

  public function get($fact_type, $action_id) {
    return $this->api->send('GET', array('actions', $fact_type, $action_id));
  }

  public function update($fact_type, $action_id, $data) {
    return $this->api->send('POST', array('actions', $fact_type, $action_id), array('body' => $data));
  }

  public function remove($fact_type, $action_id) {
    return $this->api->send('DELETE', array('actions', $fact_type, $action_id));
  }

  public function test($fact_type, $action_id, $fact_id, $force) {
    return $this->api->send('GET', array('actions', $fact_type, $action_id, 'test', $fact_id), array('body' => array('force' => $force)));
  }

  public function exec($fact_type, $action_id, $fact_id, $stage) {
    return $this->api->send('GET', array('actions', $fact_type, $action_id, 'exec', $fact_id, $stage));
  }

  public function history($fact_type, $action_id, $fact_id) {
    return $this->api->send('GET', array('actions', $fact_type, $action_id, 'history', $fact_id));
  }
}
