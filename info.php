<?php

class FactionInfo {

  protected $api;

  public function __construct($api) {
    $this->api = $api;
  }

  public function create($type, $data) {
    return $this->api->send('POST', array('info', $type), array('body' => $data));
  }

  public function get_mappings($type, $options) {
    return $this->api->send('GET', array('info', $type, 'mappings'), $options);
  }

  public function get_mapping($type, $id) {
    return $this->api->send('GET', array('info', $type, 'mappings', $id));
  }

  public function update_mapping($type, $id, $data) {
    return $this->api->send('POST', array('info', $type, 'mappings', $id), array('body' => $data));
  }

  public function remove_mapping($type, $id) {
    return $this->api->send('DELETE', array('info', $type, 'mappings', $id));
  }

  public function get_schema($type) {
    return $this->api->send('GET', array('info', $type, 'schema'));
  }

  public function update_schema($type, $data) {
    return $this->api->send('POST', array('info', $type, 'schema'), array('body' => $data));
  }

  public function test_schema($type, $data) {
    return $this->api->send('POST', array('info', $type, 'schema', 'test'), array('body' => $data));
  }

  public function remove_schema($type) {
    return $this->api->send('DELETE', array('info', $type, 'schema'));
  }
}
