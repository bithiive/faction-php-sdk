<?php

class FactionHook {

  protected $api;

  public function __construct($api) {
    $this->api = $api;
  }

  public function list_all() {
    return $this->api->send('GET', array('hooks'));
  }

  public function get($id) {
    return $this->api->send('GET', array('hooks', $id));
  }

  public function update($id, $data) {
    return $this->api->send('POST', array('hooks', $id), array('body' => $data));
  }

  public function remove($id) {
    return $this->api->send('DELETE', array('hooks', $id));
  }
}
