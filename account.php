<?php

class FactionAccount {

  protected $api;

  public function __construct($api) {
    $this->api = $api;
  }

  public function create($data) {
    return $this->api->send('POST', array('account'), array('body' => $data));
  }

  public function activate($key) {
    return $this->api->send('POST', array('account', 'activate', $key));
  }

  public function get() {
    return $this->api->send('GET', array('account'));
  }

  public function update($data) {
    return $this->api->send('POST', array('account/contact'), array('body' => $data));
  }

  public function update_key($name, $data) {
    return $this->api->send('POST', array('account/key', $name), array('body' => $data));
  }

  public function remove_key($name) {
    return $this->api->send('DELETE', array('account/key', $name));
  }

  public function services() {
    return $this->api->send('GET', array('account/services'));
  }

  public function update_service($name, $data) {
    return $this->api->send('POST', array('account/services', $name), array('body' => $data));
  }

  public function remove_service($name) {
    return $this->api->send('DELETE', array('account/services', $name));
  }

  public function trigger_service_update() {
    return $this->api->send('POST', array('account/update-services'));
  }
}
