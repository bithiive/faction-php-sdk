<?php

require_once('account.php');
require_once('info.php');
require_once('fact.php');
require_once('hook.php');
require_once('action.php');
require_once('misc.php');
require_once('notification.php');

class FactionClient {
  public $host, $port, $key, $account, $info, $fact, $hook, $condition, $action, $misc, $notification;

  public function FactionClient($host = 'api.trakapo.com', $port = 80) {
    $this->host = $host;
    $this->port = $port;

    $this->account = new FactionAccount($this);
    $this->info = new FactionInfo($this);
    $this->fact = new FactionFact($this);
    $this->hook = new FactionHook($this);
    $this->action = new FactionAction($this);
    $this->misc = new FactionMisc($this);
    $this->notification = new FactionNotification($this);
  }

  public function set_key($key) {
    $this->key = (array) $key;
  }

  private function make_hash($path, $body) {
    $hash_parts = array(
      $path,
      json_encode($body),
    );

    return hash_hmac('sha256', implode('', $hash_parts), $this->key['private']);
  }

  public function send($method, $path, $options = array()) {
    // Allows for body or query to be empty
    $body = isset($options['body']) && $options['body'] != FALSE ? $options['body'] : new stdClass;
    $queryArr = isset($options['query']) && $options['query'] != FALSE ? $options['query'] : NULL;

    // Check for the path being an array
    if (is_array($path)) {
      $path = implode('/', $path);
    }

    // Ensure always just a single leading slash
    $path = '/' . preg_replace('/^\//', '', $path);

    // If auth is required
    $auth = isset($options['auth']) ? $options['auth'] : TRUE;
    $queryArr['key'] = $this->key['public'];
    if ($auth) {
      if (!$this->key) {
        throw new Exception('This route is HMAC-authorised, but no keypair has been provided');
      }

      $secure = isset($options['secure']) ? $options['secure'] : TRUE;
      if ($this->key['private'] && $secure != FALSE) {
        $queryArr['hash'] = $this->make_hash($path, $body);
      }
    }

    $query = http_build_query($queryArr);

    $body = json_encode($body);

    $ch = curl_init($this->host . ':' . $this->port . $path . '?' .  $query);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));

    switch ($method) {
      case "POST":
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        break;

      case "PUT":
        $size = strlen($body);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-length: ' . $size, 'Content-type: application/json'));
        break;

      case "DELETE":
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        break;

      case "GET":
        curl_setopt($ch, CURLOPT_URL, $this->host . ':' . $this->port . $path . '?' .  $query . '&body=' . urlencode($body));
        break;

      default:
        throw new Exception('Unknown request method');
        break;
    }

    return json_decode(curl_exec($ch));
  }
}
